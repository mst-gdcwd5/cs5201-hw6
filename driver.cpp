// Geoffrey Cline 
// CS5101, HW6, SS17

#include "diagMatrix.h"
#include "dMatrix.h"
#include "tMatrix.h"
#include "specialMult.h"
#include "solve.h"

using namespace std;

//solves tridiag matrix from streak
//PRE: reads in size, then matrix, then vector
//PST: outputs to cout solution vector
void drive_tri(istream& is);

//solves symmetric matrix from streak
//PRE: reads in size, then matrix, then vector
//PST: outputs to cout solution vector
void drive_symm(istream& is);

using N = double;

int main(int argc, char *argv[])
{
    string ifname;
    ifstream in;

    std::cout << "# Hello, World Y'All" << std::endl;

    if( argc < 2)
    {
        ifname = "data/t3.txt";
    }
    else
        ifname = argv[1];


    cout << "# Opening File: " << ifname << endl;

    try{

        in.open(ifname);

        cout << "# TriDiagional Matrix" << endl;
        drive_tri(in);

        cout << "# Symmetric Matrix" << endl;
        drive_symm(in);

        in.close();
    }
    catch(const std::runtime_error& e)
    {
        cout << "# runtime error: " << e.what() << endl;
    }
    catch(const std::logic_error& e)
    {
        cout << "# logic error: "  << e.what() << endl;
    }

    

    return 0;

}

void drive_symm(istream& is){

    mSolve fn;


    sMatrix<N> tc1;
    gVector<N> tc2;

    fileInput(is, tc1);

    tc2.resize(tc1.size());

    is >> tc2;

    auto tc3 = fn(tc1, tc2);
    auto tc4 = decomp(tc1);

    cout << "# Tridiagional Decomp" << endl;
    for(int i = 0; i < tc4.size(); i++){
        cout << "# ";
        for(int j = 0; j < tc4.size(); j++)
            cout << tc4.get(i,j) << " ";
        cout << endl;
    }

    cout << "# Tridiagional Solve" << endl;

    cout << tc3 << endl;
}

void drive_tri(istream& is){

    mSolve fn;


    tMatrix<N> tc1;
    gVector<N> tc2;

    fileInput(is, tc1);

    tc2.resize(tc1.size());

    is >> tc2;

    auto tc3 = fn(tc1, tc2);
    
    cout << tc3 << endl;
}




