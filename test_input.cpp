#include <boost/test/unit_test.hpp>
#include "dMatrix.h"
#include "tMatrix.h"
#include "sMatrix.h"
#include "solve.h"
#include <iostream>
#include <fstream>

using namespace std;

BOOST_AUTO_TEST_SUITE( am_input )

BOOST_AUTO_TEST_CASE( input_tri )
{
    tMatrix<float> tc1;
    gVector<float> tc2;

    ifstream is;

    is.open("data/t1.txt");

    fileInput(is, tc1);

    tc2.resize(tc1.size());

    is >> tc2;

    cout << "Input Testing!" << endl;
    cout << tc1 << endl;
    cout << tc2 << endl;

    is.close();

    auto tc3 = solve(tc1, tc2);

    cout << "finish" << endl;
    cout << tc3 << endl;

}

BOOST_AUTO_TEST_CASE( input_sym )
{
    cout << "begin symm" << endl;

    sMatrix<float> tc1;
    gVector<float> tc2;

    ifstream is;

    is.open("data/t2.txt");

    fileInput(is, tc1);

    tc2.resize(tc1.size());

    is >> tc2;

    cout << "Input Testing!" << endl;
    cout << tc1 << endl;
    cout << tc2 << endl;

    is.close();

    auto tc3 = solve(tc1, tc2);

    cout << "finish" << endl;
    cout << tc3 << endl;

}

BOOST_AUTO_TEST_SUITE_END()