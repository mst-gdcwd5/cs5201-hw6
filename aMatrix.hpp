#pragma once

template<typename T>
dMatrix<T> operator*(const aMatrix<T>& l, const aMatrix<T>& r){
    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    int size = l.size();

    dMatrix<T> result(size);

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            result.element(i,j) = 0;
            int st = max(l.nt_row_start(i),r.nt_col_start(j));
            int end = min(l.nt_row_end(i),r.nt_col_end(j));
            for(int k = st; k < end; k++)
            {
                ops++;
                result.element(i,j) += l.get(i,k) * r.get(k,j);
            }
        }

    cout << "# [" << l.size() << "," << r.size() <<"] operations: " << ops << endl;
    return result;

}

template<typename T>
dMatrix<T> operator+(const aMatrix<T>& l, const aMatrix<T>& r){
    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix add");

    int size = l.size();

    dMatrix<T> result(size); //aMatrix -> dMatrix?

    result = l;

    for(int i = 0; i < size; i++)
        for(int j = r.nt_row_start(i); j < r.nt_row_end(i); j++)
        {
            result.element(i,j) += r.get(i,j);
        }

    return result;
}

template<typename T>
dMatrix<T> operator*(const aMatrix<T>& l, const T& r){

    dMatrix<T> result(l);


    for(int i = 0; i < l.size(); i++)
        for(int j = l.nt_row_start(i); j < l.nt_row_end(i); j++)
        {

            result.element(i,j) *= r;
        }

    return result;
}

template<typename T>
dMatrix<T> transpose(const aMatrix<T>& l){
    dMatrix<T> result(l.size());

    for(int i = 0; i < l.size(); i++)
        for(int j = 0; j < l.size(); j++)
        {
            result.element(j,i) = l.get(i,j);
        }

    return result;
}

template<typename T>
ostream& operator<<(ostream& os, const aMatrix<T>& aM){
    for(int i = 0; i < aM.size(); i++){
        for(int j = 0; j < aM.size(); j++)
            os << aM.get(i,j) << " ";
        os << endl;
    }

    return os;
}

template<typename T>
dMatrix<T> operator+(const dMatrix<T>& l, const dMatrix<T>& r){
    const aMatrix<T>& al = l;
    const aMatrix<T>& ar = r;
    return (al + ar);
}

template<typename T>
dMatrix<T> operator-(const aMatrix<T>& l, const aMatrix<T>& r){
    return (l + (r * -1));
}

template<typename T, typename R>
gVector<R> operator*(const aMatrix<T>& l, const gVector<R>& r){
    if(r.size() != l.size())
        throw std::logic_error("invalid matrix vector multiply");


    gVector<R> result(l.size());

    for(int i = 0; i < l.size(); i++)
    {
        result[i] = 0;
        for(int j = l.nt_row_start(i); j < l.nt_row_end(i); j++)
            result[i] += r.get(i) * l.get(i,j);
    }

    return result;
}

template<typename T>
dMatrix<T> operator+(const aMatrix<T>& l, const T& r){

    dMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
dMatrix<T> operator-(const aMatrix<T>& l, const T& r){

    dMatrix<T> result(l);

    result -= r;

    return result;
}

template<typename T>
istream& operator>>(istream& is, aMatrix<T>& aM)
{
    const int sz = aM.size();

    T* data = new T [sz*sz];

    for(int i = 0; i < sz*sz; i++){
        is >> data[i];
    }

    dMatrix<T> hold(sz, data);

    for(int i = 0; i < sz; i++)
        for(int j = aM.nt_row_start(i); j < aM.nt_row_end(i); j++){
            aM.element(i,j) = hold.get(i,j);
        }

    return is;
}

template<typename T>
void fileInput(const string l, aMatrix<T>& r){
    std::ifstream is;

    //error?

    is.open(l);

    fileInput(is,r);

    is.close();
}

template<typename T>
void fileInput(istream& is, aMatrix<T>& r){
    int newsz;

    is >> newsz;

    r.resize(newsz);

    is >> r;

}