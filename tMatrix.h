#pragma once

#include "aMatrix.h"

template<typename T>
class tMatrix : public aMatrix<T>
{
    T* m_data;
    int m_size;

    void move(tMatrix<T> & mRHS);

    public:

    virtual inline int map(const int i, const int j) const;

    virtual inline bool valid(const int i, const int j) const;

    virtual inline bool valid(const int i) const;

    virtual inline bool defined(const int i, const int j) const;

    virtual ~tMatrix();

    tMatrix(const int i = M_DEFAULT_SIZE);

    tMatrix(const tMatrix<T>& r);

    tMatrix(const int size, const T data[]);

    virtual tMatrix<T>& operator=(const tMatrix<T>& r);

    void readin(const int size, const T data[]);

    // inherited thangs
    virtual T& element(const int i, const int j);

    virtual T& element(const int i);

    virtual T get(const int i, const int j) const;

    virtual T get(const int i) const;

    //assumes all of these will be square
    virtual void resize(const int i);

    virtual int size() const;

    //access indecies of non-trival elements
    virtual int nt_row_start(const int i) const;

    virtual int nt_row_end(const int i) const;

    virtual int nt_col_start(const int i) const;

    virtual int nt_col_end(const int i) const;

    //fast operators
    virtual tMatrix<T>& operator*=(const T& r);

    virtual tMatrix<T>& operator+=(const T& r);

    virtual tMatrix<T>& operator-=(const T& r);

    virtual tMatrix<T>& operator+=(const tMatrix<T>& r);

    virtual tMatrix<T>& operator-=(const tMatrix<T>& r);

};

template<typename T>
tMatrix<T> operator*(const tMatrix<T>& l, const T& r);

template<typename T>
tMatrix<T> operator+(const tMatrix<T>& l, const tMatrix<T>& r);

template<typename T>
tMatrix<T> operator-(const tMatrix<T>& l, const tMatrix<T>& r);

template<typename T>
tMatrix<T> operator+(const tMatrix<T>& l,  const T& r);

template<typename T>
tMatrix<T> operator-(const tMatrix<T>& l,  const T& r);

#include "tMatrix.hpp"