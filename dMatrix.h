#pragma once

#include "aMatrix.h"

template<typename T>
class dMatrix : public aMatrix<T>
{
private:
    T* m_data;
    int m_size;


    inline int count() const;

    void move(const dMatrix<T>& mRHS);

public:

    virtual inline int map(const int i, const int j) const;

    virtual inline bool valid(const int i) const;

    virtual inline bool valid(const int i, const int j) const;

    virtual inline bool defined(const int i, const int j) const;


    virtual ~dMatrix();

    dMatrix(const int i = M_DEFAULT_SIZE);

    dMatrix(const dMatrix<T>& mRHS);


    void readin(const int size, const T data[] );

    dMatrix(const int size, const T data [] );

    dMatrix(const dMatrix<T>&& mRHS);

    dMatrix(const aMatrix<T>& r);

    virtual dMatrix<T>& operator=(const aMatrix<T>& r);

    virtual dMatrix<T>& operator=(const dMatrix<T>& r);

    //set&get
    virtual T& element(const int i, const int j);

    virtual T& element(const int i);

    virtual T get(const int i, const int j) const;

    virtual T get(const int i) const;

    //assumes all of these will be square
    virtual void resize(const int i );

    virtual int size() const;

    //access indecies of non-trival elements
    virtual int nt_row_start(const int i) const;

    virtual int nt_row_end(const int i) const;

    virtual int nt_col_start(const int i) const;

    virtual int nt_col_end(const int i) const;

    //fast operators
    virtual dMatrix<T>& operator*=(const T& r);

    virtual dMatrix<T>& operator+=(const T& r);

    virtual dMatrix<T>& operator-=(const T& r);

    virtual dMatrix<T>& operator+=(const dMatrix<T>& r);

    virtual dMatrix<T>& operator-=(const dMatrix<T>& r);

};




#include "dMatrix.hpp"