#pragma once

#include "aMatrix.h"

template<typename T>
class uMatrix : public aMatrix<T>
{
    T* m_data;
    int m_size;



public:

    virtual inline int map(const int i, const int j) const;


    virtual inline bool valid(const int i, const int j) const;

    virtual inline bool valid(const int i) const;

    virtual inline bool defined(const int i, const int j) const;

    void move(dMatrix<T>& mRHS);

    virtual ~uMatrix();

    uMatrix(const int i = M_DEFAULT_SIZE);

    uMatrix(const uMatrix<T>& r);

    uMatrix(const int size, const T data []);

    uMatrix(const uMatrix<T>&& r);

    virtual uMatrix<T>& operator=(const uMatrix<T>& r);

    void readin(const int size, const T data[]);

    //inherited things:

    //set&get
    virtual T& element(const int i, const int j);

    virtual T& element(const int i);

    virtual T get(const int i, const int j) const;

    virtual T get(const int i) const;

    //assumes all of these will be square
    virtual void resize(const int i);

    virtual int size() const;

    //access indecies of non-trival elements
    virtual int nt_row_start(const int i) const;

    virtual int nt_row_end(const int i) const;

    virtual int nt_col_start(const int i) const;

    virtual int nt_col_end(const int i) const;

    //fast operators
    virtual uMatrix<T>& operator*=(const T& r);

    virtual uMatrix<T>& operator+=(const T& r);

    virtual uMatrix<T>& operator-=(const T& r);

    virtual uMatrix<T>& operator+=(const uMatrix<T>& r);

    virtual uMatrix<T>& operator-=(const uMatrix<T>& r);


};

template<typename T>
uMatrix<T> operator*(const uMatrix<T>& l, const T& r);

template<typename T>
uMatrix<T> operator*(const uMatrix<T>& l, const uMatrix<T>& r);

template<typename T>
uMatrix<T> operator+(const uMatrix<T>& l, const uMatrix<T>& r);

template<typename T>
uMatrix<T> operator-(const uMatrix<T>& l, const uMatrix<T>& r);

template<typename T>
uMatrix<T> operator+(const uMatrix<T>& l,  const T& r);

template<typename T>
uMatrix<T> operator-(const uMatrix<T>& l,  const T& r);

template<typename T>
istream& operator>>(istream& is, aMatrix<T>& aM);

#include "uMatrix.hpp"