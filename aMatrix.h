#pragma once

#include <iostream>
#include <fstream>

#include "gVector/gVector.h"

const int M_DEFAULT_SIZE = 5;

using namespace std;

//return type? always returning references?
//can't template virtual?
//shared implementation?
//what other fns needed in base class?

//using T = int;

template<typename T>
class dMatrix;

template<typename T>
class aMatrix{
public:

    virtual ~aMatrix() {}

    //access checks

    //return true if the index is within the matrix
    //PRE:
    //PST: 
    virtual inline bool valid(const int i, const int j) const = 0;

    //return true if the index is within the matrix
    //PRE:
    //PST: 
    virtual inline bool valid(const int i) const = 0;

    //return true if the index is defined within the matrix
    //i.e., memory allocated for that location
    //PRE:
    //PST: 
    virtual inline bool defined(const int i, const int j) const = 0;

    //returns reference to index in matrix
    //PRE: index is valid, AND index is defined
    //PST: reference returned
    virtual T& element(const int i, const int j) = 0;

    //returns reference to index in matrix
    //PRE: index is valid, AND index is defined
    //PST: reference returned
    virtual T& element(const int i) = 0;

    //returns copy of data from index in matrix
    //PRE: index is valid
    //PST: copy returned
    virtual T get(const int i, const int j) const = 0;

    //returns copy of data from index in matrix
    //PRE: index is valid
    //PST: copy returned
    virtual T get(const int i) const = 0;

    //updates size of matrix (will be square)
    //PRE: i >= 0
    //PST: data freed, data reallocated
    virtual void resize(const int i) = 0; 

    //returns current size of matrix
    //PRE:
    //PST: 
    virtual int size() const = 0;

    //access indecies of non-trival elements

    //returns first defined index of a row
    //PRE: i is a valid row index
    //PST: 
    virtual int nt_row_start(const int i) const = 0;

    //returns first undefined index of a row
    //PRE: i is a valid row index
    //PST: 
    virtual int nt_row_end(const int i) const = 0;

    //returns first defined index of a col
    //PRE: i is a valid col index
    //PST: 
    virtual int nt_col_start(const int i) const = 0;

    //returns first undefined index of a col
    //PRE: i is a valid col index
    //PST: 
    virtual int nt_col_end(const int i) const = 0;


};

//returns multiplication of two matricies
//PRE: sizes of both are equal, operator* for T
//PST: 
template<typename T>
dMatrix<T> operator*(const aMatrix<T>& l, const aMatrix<T>& r);

//returns scale of a matrix
//PRE: operator+ for T
//PST: 
template<typename T>
dMatrix<T> operator*(const aMatrix<T>& l, const T& r);

//returns basic add of a matrix
//PRE: operator+ for T
//PST: 
template<typename T>
dMatrix<T> operator+(const aMatrix<T>& l, const aMatrix<T>& r);

//returns addition of two matricies
//PRE: operator+ for T
//PST: 
template<typename T>
dMatrix<T> operator+(const dMatrix<T>& l, const dMatrix<T>& r);

//returns subtraction of two matricies
//PRE: operator- for T
//PST: 
template<typename T>
dMatrix<T> operator-(const aMatrix<T>& l, const aMatrix<T>& r);

//returns transpose of matrix
//PRE: 
//PST: 
template<typename T>
dMatrix<T> transpose(const aMatrix<T>& l);

//outputs matrix
//PRE: operator<< for T
//PST: 
template<typename T>
ostream& operator<<(ostream& os, const aMatrix<T>& aM);

//matrix, vector multiplication
//PRE: operator* for T
//PST: 
template<typename T, typename R>
gVector<R> operator*(const aMatrix<T>& l, const gVector<R>& r);

//input into matrix
//PRE: operator>> for T, size of aM dictates expected input
//PST: 
template<typename T>
istream& operator>>(istream& is, aMatrix<T>& aM);

//file input into matrix
//PRE: operator>> for T, l filename exists
//PST: size of r updated
template<typename T>
void fileInput(const string l, aMatrix<T>& r);

//file input into matrix
//PRE: operator>> for T
//PST: size of r updated
template<typename T>
void fileInput(istream& is, aMatrix<T>& r);

#include "aMatrix.hpp"
