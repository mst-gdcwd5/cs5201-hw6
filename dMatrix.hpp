#pragma once

template<typename T>
inline int dMatrix<T>::map(const int i, const int j) const  {
    return i*m_size + j;
}

template<typename T>
inline bool dMatrix<T>::valid(const int i) const  {
    return i < (m_size * m_size);
}

template<typename T>
inline int dMatrix<T>::count() const {
    return m_size * m_size;
}

template<typename T>
inline bool dMatrix<T>::valid(const int i, const int j) const  {
    return i >= 0 && j >= 0 && j < m_size && i < m_size;
}

template<typename T>
void dMatrix<T>::move(const dMatrix<T>& mRHS){
    resize(0);
    m_data = mRHS.m_data;
    m_size = mRHS.m_size;
}

template<typename T>
dMatrix<T>::~dMatrix() {
    resize(0);
}

template<typename T>
dMatrix<T>::dMatrix(const int i){
    m_size = 0;
    resize(i);
}

template<typename T>
void dMatrix<T>::readin(const int size, const T data[] ){
    resize(size);

    for(int i = 0; i < count(); i++)
        element(i) = data[i];

}

template<typename T>
dMatrix<T>::dMatrix(const int size, const T data [] ){
    m_size = 0;
    readin(size, data);
}

template<typename T>
dMatrix<T>::dMatrix(const dMatrix<T>&& mRHS){
    m_size = 0;
    move(mRHS);
}

template<typename T>
dMatrix<T>& dMatrix<T>::operator=(const aMatrix<T>& r){
    resize(r.size());


    for(int i = 0; i < size(); i++)
        for(int j = 0; j < size(); j++)
        {
            element(i,j) = r.get(i,j);
        }

    return *this;
}


template<typename T>
dMatrix<T>::dMatrix(const dMatrix<T>& mRHS){
    m_size = 0;
    resize(mRHS.size());
    
    const int count = size() * size();
    for(int i = 0; i < count; i++){
        element(i) = mRHS.get(i);
    }

}



template<typename T>
T& dMatrix<T>::element(const int i, const int j) {
    if(!valid(i,j))
        throw std::logic_error("invalid matrix access");
    return m_data[map(i,j)];
}

template<typename T>
T& dMatrix<T>::element(const int i) {
    if(!valid(i))
        throw std::logic_error("invalid matrix access");
    return m_data[i];
}

template<typename T>
T dMatrix<T>::get(const int i, const int j) const {
    if(!valid(i,j))
        throw std::logic_error("invalid matrix access");
    return m_data[map(i,j)];
}

template<typename T>
T dMatrix<T>::get(const int i) const {
    if(!valid(i))
        throw std::logic_error("invalid matrix access");
    return m_data[i];
}

template<typename T>
void dMatrix<T>::resize(const int i ){
    if(m_size > 0)
    {
        delete [] m_data;
        m_data = nullptr;
    }
        
    if(i > 0)
    {
        m_data = new T [i * i];
        m_size = i;
    }
}

template<typename T>
int dMatrix<T>::size() const {
    return m_size;
}


//access indecies of non-trival elements
template<typename T>
int dMatrix<T>::nt_row_start(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid col access");

    return 0;
}

template<typename T>
int dMatrix<T>::nt_row_end(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid col access");

    return size();
}

template<typename T>
int dMatrix<T>::nt_col_start(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return 0;
}

template<typename T>
int dMatrix<T>::nt_col_end(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return size();
}

template<typename T>
inline bool dMatrix<T>::defined(const int i, const int j) const{
    return valid(i,j);
}

template<typename T>
dMatrix<T>::dMatrix(const aMatrix<T>& r){
    m_size = 0;
    resize(r.size());

    for(int i = 0; i < size(); i++)
        for(int j = 0; j < size(); j++)
        {
            element(i,j) = r.get(i,j);
        }

}

template<typename T>
dMatrix<T>& dMatrix<T>::operator=(const dMatrix<T>& r){
    resize(r.size());

    for(int i = 0; valid(i); i++)
        element(i) = r.get(i);

    return *this;
}

template<typename T>
dMatrix<T>& dMatrix<T>::operator*=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) *= r;

    return *this;
}

template<typename T>
dMatrix<T>& dMatrix<T>::operator+=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) += r;

    return *this;
}

template<typename T>
dMatrix<T>& dMatrix<T>::operator-=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) -= r;

    return *this;
}

template<typename T>
dMatrix<T>& dMatrix<T>::operator+=(const dMatrix<T>& r){

    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) += r.get(i);

    return *this;
}

template<typename T>
dMatrix<T>& dMatrix<T>::operator-=(const dMatrix<T>& r){


    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) -= r.get(i);

    return *this;
}