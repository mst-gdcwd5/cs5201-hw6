#pragma once

#include "aMatrix.h"

template<typename T>
class diagMatrix : public aMatrix<T>
{
    T* m_data;
    int m_size;

    //steals data of mRHS
    //pre:
    //pst: mRHS updated to null, calling has size/data of old RHS
    void move(diagMatrix<T>& mRHS);

    public:

    //destructor!
    //pre:
    //pst: data deallocated
    virtual ~diagMatrix();

    //constructs with size
    //pre:
    //pst: data allocated, but not zeroed
    diagMatrix(const int i = M_DEFAULT_SIZE);

    //copy constructor
    //pre:
    //pst: data and size deep copy
    diagMatrix(const diagMatrix<T>& r);

    //array constructor
    //pre: data has elements >= defined elements of diag
    //pst: data read in from array
    diagMatrix(const int size, const T data []);

    //this is the moment I stopped caring about this course.
    diagMatrix(diagMatrix<T>&& r);

    virtual inline int map(const int i, const int j) const;

    virtual inline bool valid(const int i, const int j) const;

    virtual inline bool valid(const int i) const ;

    virtual inline bool defined(const int i, const int j) const;

    virtual diagMatrix<T>& operator=(const diagMatrix<T>& r);

    virtual void readin(const int size, const T data[]);

    virtual T& element(const int i, const int j);

    virtual T& element(const int i);

    virtual T get(const int i, const int j) const;

    virtual T get(const int i) const;

    virtual void resize(const int i);

    virtual int size() const;

    virtual int nt_row_start(const int i) const;

    virtual int nt_row_end(const int i) const;

    virtual int nt_col_start(const int i) const;

    virtual int nt_col_end(const int i) const;

    //fast operators
    virtual diagMatrix<T>& operator*=(const T& r);

    virtual diagMatrix<T>& operator+=(const T& r);

    virtual diagMatrix<T>& operator-=(const T& r);

    virtual diagMatrix<T>& operator+=(const diagMatrix<T>& r);

    virtual diagMatrix<T>& operator-=(const diagMatrix<T>& r);

};

//Template Specalizations
//see aMatrix.h
template<typename T>
diagMatrix<T> operator*(const diagMatrix<T>& l, const T& r);

template<typename T>
diagMatrix<T> operator+(const diagMatrix<T>& l, const diagMatrix<T>& r);

template<typename T>
diagMatrix<T> operator-(const diagMatrix<T>& l, const diagMatrix<T>& r);

template<typename T>
diagMatrix<T> operator+(const diagMatrix<T>& l, const T& r);

template<typename T>
diagMatrix<T> operator-(const diagMatrix<T>& l, const T& r);

#include "diagMatrix.hpp"


