#include <boost/test/unit_test.hpp>
#include "diagMatrix.h"
#include "dMatrix.h"

using namespace std;

BOOST_AUTO_TEST_SUITE( diagm_internal )

BOOST_AUTO_TEST_CASE( diagm_construct )
{
    const int d1 [] = {1, 2, 3, 4 };
    const int d1_size = 4;
    const int d2 [] = {1, 0, 0, 0, 
                       0, 2, 0, 0,
                       0, 0, 3, 0,
                       0, 0, 0, 4 };

    diagMatrix<int> tc1(d1_size, d1);
    diagMatrix<int> tc2(tc1);
    diagMatrix<int> tc3;

    tc3 = tc2;


    BOOST_REQUIRE_EQUAL(tc1.size(), d1_size);
    BOOST_REQUIRE_EQUAL(tc2.size(), d1_size);
    BOOST_REQUIRE_EQUAL(tc3.size(), d1_size);

    cout << tc1 << endl;

    int count = 0;
    for(int i = 0; i < tc1.size(); i++)
        for(int j = 0; j < tc1.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc2.get(i,j), d2[count]);
            BOOST_CHECK_EQUAL(tc1.get(i,j), d2[count]);
            BOOST_CHECK_EQUAL(tc3.get(i,j), d2[count]);
            count++;
        }

}

BOOST_AUTO_TEST_CASE( diagm_scale )
{
    const int d1 [] = {1, 2, 3, 4 };
    const int d1_size = 4;
    const int d2 [] =   {2, 0, 0, 0, 
                         0, 4, 0, 0,
                         0, 0, 6, 0,
                         0, 0, 0, 8 };

    diagMatrix<int> tc1(8);

    tc1.readin(d1_size, d1);

    diagMatrix<int> tc2;

    tc2 = tc1 * 2;

    BOOST_REQUIRE_EQUAL(tc2.size(), d1_size);

    int count = 0;
    for(int i = 0; i < tc2.size(); i++)
        for(int j = 0; j < tc2.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc2.get(i,j), d2[count]);
            count++;
        }

}

BOOST_AUTO_TEST_CASE( diagm_fast_scale )
{
    const int d1 [] = {1, 2, 3, 4 };
    const int d1_size = 4;
    const int d2 [] =   {2, 0, 0, 0, 
                         0, 4, 0, 0,
                         0, 0, 6, 0,
                         0, 0, 0, 8 };

    diagMatrix<int> tc1(8);

    tc1.readin(d1_size, d1);

    diagMatrix<int> tc2(tc1);

    tc2 *= 2;

    BOOST_REQUIRE_EQUAL(tc2.size(), d1_size);

    int count = 0;
    for(int i = 0; i < tc2.size(); i++)
        for(int j = 0; j < tc2.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc2.get(i,j), d2[count]);
            count++;
        }

}



BOOST_AUTO_TEST_SUITE_END()

