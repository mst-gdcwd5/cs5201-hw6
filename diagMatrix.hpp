#pragma once

template<typename T>
void diagMatrix<T>::move(diagMatrix<T>& mRHS){
        m_data = mRHS.m_data;
        m_size = mRHS.m_size;
        mRHS.m_data = nullptr;
        mRHS.m_size = 0;
}

template<typename T>
inline int diagMatrix<T>::map(const int i, const int j) const {
        if(!valid(i,j))
            throw std::logic_error("invalid access 5");
        return i;
}

template<typename T>
inline bool diagMatrix<T>::valid(const int i, const int j) const{
    return i >= 0 && j >= 0 && j < m_size && i < m_size;
}

template<typename T>
inline bool diagMatrix<T>::valid(const int i) const {
    return i < m_size && i >= 0;
}

template<typename T>
inline bool diagMatrix<T>::defined(const int i, const int j) const {
    return i >= 0 && i == j;
}

template<typename T>
diagMatrix<T>::~diagMatrix() {
        resize(0);
    }

template<typename T>
diagMatrix<T>::diagMatrix(const int i){
    m_size = 0;
    resize(i);
}

template<typename T>
diagMatrix<T>::diagMatrix(const diagMatrix<T>& r){
    m_size = 0;
    resize(r.size());

    for(int i = 0; valid(i); i++)
        element(i) = r.get(i);
}

template<typename T>
diagMatrix<T>::diagMatrix(const int size, const T data []){
    m_size = 0;
    readin(size, data);

}

template<typename T>
diagMatrix<T>::diagMatrix(diagMatrix<T>&& r){
    m_size = 0;
    move(r);
}

template<typename T>
diagMatrix<T>& diagMatrix<T>::operator=(const diagMatrix<T>& r){
    resize(r.size());

    for(int i = 0; valid(i); i++)
        element(i) = r.get(i);

    return *this;
}

template<typename T>
void diagMatrix<T>::readin(const int size, const T data[]){
    resize(size);

    for(int i = 0; valid(i); i++)
        element(i) = data[i];
}

template<typename T>
T& diagMatrix<T>::element(const int i, const int j){
    if(!valid(i,j) || !defined(i,j))
        throw std::logic_error("invalid access 3");

    return m_data[map(i,j)];
}

template<typename T>
T& diagMatrix<T>::element(const int i){
    if(!valid(i))
        throw std::logic_error("invalid access 2");

    return m_data[i];
}

template<typename T>
T diagMatrix<T>::get(const int i, const int j) const {
    if(!valid(i,j))
        throw std::logic_error("invalid access 1");


    if(defined(i,j))
        return m_data[map(i,j)];
    else
        return 0;
    

}

template<typename T>
T diagMatrix<T>::get(const int i) const {
    if(!valid(i))
        throw std::logic_error("invalid access 4");

    return m_data[i];
}

template<typename T>
void diagMatrix<T>::resize(const int i) {
        
    
    if(m_size > 0)
    {
        delete [] m_data;
        m_data = nullptr;
    }
    if(i > 0)
    {
        m_size = i;
        m_data = new T [i];
    }
}

template<typename T>
int diagMatrix<T>::size() const {
    return m_size;
}

template<typename T>
int diagMatrix<T>::nt_row_start(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid row access");

    return i;
}

template<typename T>
int diagMatrix<T>::nt_row_end(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid row access");

    return i + 1;
}

template<typename T>
int diagMatrix<T>::nt_col_start(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return i;
}

template<typename T>
int diagMatrix<T>::nt_col_end(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return i + 1;
}


template<typename T>
diagMatrix<T>& diagMatrix<T>::operator*=(const T& r){
    
    for(int i = 0; valid(i); i++)
        element(i) *= r;

    return *this;
}

template<typename T>
diagMatrix<T>& diagMatrix<T>::operator+=(const T& r){

    

    for(int i = 0; valid(i); i++)
        element(i) += r;

    return *this;
}

template<typename T>
diagMatrix<T>& diagMatrix<T>::operator-=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) -= r;

    return *this;
}

template<typename T>
diagMatrix<T>& diagMatrix<T>::operator+=(const diagMatrix<T>& r){

    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) += r.get(i);

    return *this;
}

template<typename T>
diagMatrix<T>& diagMatrix<T>::operator-=(const diagMatrix<T>& r){

    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) -= r.get(i);

    return *this;
}

template<typename T>
diagMatrix<T> operator*(const diagMatrix<T>& l, const T& r){

    diagMatrix<T> result(l);

    result *= r;

    return result;
}

template<typename T>
diagMatrix<T> operator+(const diagMatrix<T>& l, const diagMatrix<T>& r){

    diagMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
diagMatrix<T> operator-(const diagMatrix<T>& l, const diagMatrix<T>& r){

    diagMatrix<T> result(l);

    result -= r;

    return result;
}

template<typename T>
diagMatrix<T> operator+(const diagMatrix<T>& l, const T& r){

    diagMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
diagMatrix<T> operator-(const diagMatrix<T>& l, const T& r){

    diagMatrix<T> result(l);

    result -= r;

    return result;
}