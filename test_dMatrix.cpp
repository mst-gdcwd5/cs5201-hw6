#include <boost/test/unit_test.hpp>
#include "dMatrix.h"

using namespace std;

BOOST_AUTO_TEST_SUITE( dm_internal )

BOOST_AUTO_TEST_CASE( dm_construct )
{
	dMatrix<int> tc1;

    int expected_size = 5;

    BOOST_CHECK_EQUAL(tc1.size(), expected_size);

    dMatrix<int> tc2(50);
    dMatrix<int> tc3(tc2);

    expected_size = 50;

    BOOST_CHECK_EQUAL(tc2.size(), expected_size);
    BOOST_CHECK_EQUAL(tc3.size(), expected_size);

	const int d1 [] = {5, 6, 7, 8};

    dMatrix<int> tc4(2,d1);

    cout << tc4 << endl;

    expected_size = 2;

    BOOST_REQUIRE_EQUAL(tc4.size(), expected_size);

    int count = 0;
    for(int i = 0; i < tc4.size(); i++)
        for(int j = 0; j < tc4.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc4.get(i,j), d1[count++]);
        }


}


BOOST_AUTO_TEST_CASE( dm_add )
{

    const int d1 [] = {5, 6, 7, 8};
    const int d2 [] = {10, 12, 14, 16};
    const int size = 2;

    dMatrix<int> tc1(size,d1);
    dMatrix<int> tc2(size,d1);

    dMatrix<int> tc3(tc1 + tc2);

    //assignment operator?


    BOOST_REQUIRE_EQUAL(tc3.size(), size);


    int step = 0;
    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j), d2[step++]);
        }


}

BOOST_AUTO_TEST_CASE( dm_scale )
{

    const int d1 [] = {5, 6, 7, 8};
    const int d2 [] = {10, 12, 14, 16};
    const int size = 2;

    dMatrix<int> tc1(size,d1);


    dMatrix<int> tc3(tc1 * 2);

    //assignment operator?


    BOOST_REQUIRE_EQUAL(tc3.size(), size);



    int step = 0;
    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j), d2[step++]);
        }


}

BOOST_AUTO_TEST_CASE( dm_transpose )
{

    const int d1 [] = {1, 2, 3, 4};
    const int d2 [] = {1, 3, 2, 4};
    const int size = 2;

    dMatrix<int> tc1(size,d1);

    dMatrix<int> tc3(transpose(tc1));

    BOOST_REQUIRE_EQUAL(tc3.size(), size);


    int step = 0;
    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j), d2[step++]);
        }


}

BOOST_AUTO_TEST_CASE( dm_mult )
{

    const int d1 [] = {1, 2, 3, 4};
    const int d2 [] = {2, 0, 1, 2};
    const int d3 [] = {4, 4, 10, 8};
    const int d4 [] = {2, 4, 7, 10};
    const int size = 2;

    dMatrix<int> tc1(size,d1);
    dMatrix<int> tc2(size,d2);

    dMatrix<int> tc3(tc1 * tc2);
    dMatrix<int> tc4(tc2 * tc1);

    BOOST_REQUIRE_EQUAL(tc3.size(), size);
    BOOST_REQUIRE_EQUAL(tc4.size(), size);


    int step = 0;
    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j), d3[step]);
            BOOST_CHECK_EQUAL(tc4.get(i,j), d4[step]);
            step++;
        }


}

BOOST_AUTO_TEST_CASE( dm_subtract )
{

    const int d1 [] = {1, 2, 3, 4};
    const int d2 [] = {2, 0, 1, 2};
    const int d3 [] = {-1, 2, 2, 2};
    const int d4 [] = {1, -2, -2, -2};
    const int size = 2;

    dMatrix<int> tc1(size,d1);
    dMatrix<int> tc2(size,d2);

    dMatrix<int> tc3(tc1 - tc2);
    dMatrix<int> tc4(tc2 - tc1);

    BOOST_REQUIRE_EQUAL(tc3.size(), size);
    BOOST_REQUIRE_EQUAL(tc4.size(), size);


    int step = 0;
    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j), d3[step]);
            BOOST_CHECK_EQUAL(tc4.get(i,j), d4[step]);
            step++;
        }


}




BOOST_AUTO_TEST_SUITE_END()