#pragma once

#include "aMatrix.h"

template<typename T>
class lMatrix : public aMatrix<T>
{
private:
    T* m_data;
    int m_size;



    void move(dMatrix<T>& mRHS);

public:

    virtual inline int map(const int i, const int j) const;

    virtual inline bool valid(const int i, const int j) const;

    virtual inline bool valid(const int i ) const;

    virtual inline bool defined(const int i, const int j) const;

    virtual ~lMatrix();


    lMatrix(const int i = M_DEFAULT_SIZE);

    lMatrix(const lMatrix<T>& r);

    lMatrix(const int size, const T data[]);

    virtual lMatrix<T>& operator=(const lMatrix<T>& r);

    void readin(const int size, const T data []);

    //inherited things :)

    virtual T& element(const int i, const int j);

    virtual T& element(const int i);

    virtual T get(const int i, const int j) const;

    virtual T get(const int i) const;

    virtual void resize(const int i);
    
    virtual int size() const;

    //access indecies of non-trival elements
    virtual int nt_row_start(const int i) const;

    virtual int nt_row_end(const int i) const;

    virtual int nt_col_start(const int i) const;

    virtual int nt_col_end(const int i) const;
    //fast operators
    virtual lMatrix<T>& operator*=(const T& r);

    virtual lMatrix<T>& operator+=(const T& r);

    virtual lMatrix<T>& operator-=(const T& r);

    virtual lMatrix<T>& operator+=(const lMatrix<T>& r);

    virtual lMatrix<T>& operator-=(const lMatrix<T>& r);


};

template<typename T>
lMatrix<T> operator*(const lMatrix<T>& l, const T& r);

template<typename T>
lMatrix<T> operator*(const lMatrix<T>& l, const lMatrix<T>& r);

template<typename T>
lMatrix<T> operator+(const lMatrix<T>& l, const lMatrix<T>& r);

template<typename T>
lMatrix<T> operator-(const lMatrix<T>& l, const lMatrix<T>& r);

template<typename T>
lMatrix<T> operator+(const lMatrix<T>& l, const T& r);

template<typename T>
lMatrix<T> operator-(const lMatrix<T>& l, const T& r);

#include "lMatrix.hpp"