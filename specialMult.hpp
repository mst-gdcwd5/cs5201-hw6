#pragma once

template<typename T>
tMatrix<T> operator*(const diagMatrix<T>& l, const tMatrix<T>& r){

    cout << "this should be running" << endl;

    int size = l.size();

    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    tMatrix<T> result(l.size());

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            if(result.defined(i,j))
                result.element(i,j) = 0;
            int st = max(l.nt_row_start(i),r.nt_col_start(j));
            int end = min(l.nt_row_end(i),r.nt_col_end(j));
            for(int k = st; k < end; k++)
            {
                ops++;
                result.element(i,j) += l.get(i,k) * r.get(k,j);
            }
        }

    cout << "# [" << l.size() << "," << r.size() <<"] operations: " << ops << endl;

    return result;
}

template<typename T>
tMatrix<T> operator*(const tMatrix<T>& r, const diagMatrix<T>& l ){
    return l * r;
}

template<typename T>
uMatrix<T> operator*(const diagMatrix<T>& l, const uMatrix<T>& r){


    int size = l.size();

    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    uMatrix<T> result(l.size());

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            if(result.defined(i,j))
                result.element(i,j) = 0;
            int st = max(l.nt_row_start(i),r.nt_col_start(j));
            int end = min(l.nt_row_end(i),r.nt_col_end(j));
            for(int k = st; k < end; k++)
            {
                ops++;
                result.element(i,j) += l.get(i,k) * r.get(k,j);
            }
        }

    cout << "# [" << l.size() << "," << r.size() <<"] operations: " << ops << endl;

    return result;
}

template<typename T>
uMatrix<T> operator*(const uMatrix<T>& r, const diagMatrix<T>& l ){
    return l * r;
}

template<typename T>
lMatrix<T> operator*(const diagMatrix<T>& l, const lMatrix<T>& r){


    int size = l.size();

    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    lMatrix<T> result(l.size());

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            if(result.defined(i,j))
                result.element(i,j) = 0;
            int st = max(l.nt_row_start(i),r.nt_col_start(j));
            int end = min(l.nt_row_end(i),r.nt_col_end(j));
            for(int k = st; k < end; k++)
            {
                ops++;
                result.element(i,j) += l.get(i,k) * r.get(k,j);
            }
        }

    cout << "# [" << l.size() << "," << r.size() <<"] operations: " << ops << endl;

    return result;
}

template<typename T>
lMatrix<T> operator*(const lMatrix<T>& r, const diagMatrix<T>& l ){
    return l * r;
}