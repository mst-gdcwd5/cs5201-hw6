#include <boost/test/unit_test.hpp>
#include "uMatrix.h"
#include "dMatrix.h"


using namespace std;

BOOST_AUTO_TEST_SUITE( um_internal )

BOOST_AUTO_TEST_CASE( um_construct )
{

    uMatrix<int> tc1;

    int expected_size = 5;

    BOOST_CHECK_EQUAL(tc1.size(), expected_size);

    uMatrix<int> tc2(50);
    uMatrix<int> tc3(tc2);

    expected_size = 50;

    BOOST_CHECK_EQUAL(tc2.size(), expected_size);
    BOOST_CHECK_EQUAL(tc3.size(), expected_size);

    const int d1 [] = {5, 6, 7};
    const int d2 [] = {5, 6, 0, 7};

    uMatrix<int> tc4(2,d1);

    cout << tc4 << endl;

    expected_size = 2;

    BOOST_REQUIRE_EQUAL(tc4.size(), expected_size);

    int count = 0;
    for(int i = 0; i < tc4.size(); i++)
        for(int j = 0; j < tc4.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc4.get(i,j), d2[count++]);
        }

}

BOOST_AUTO_TEST_CASE( um_multiply )
{
    const int d1  [] = {5, 6, 7};
    const int d1_sz = 2;

    const int d2  [] = {5, 6, 7, 8};
    const int d2_sz = 2;

    const int d3  [] = {67, 78, 49, 56};
    const int d4  [] = {25, 72, 35, 98};


    uMatrix<int> tc1(d1_sz, d1);
    dMatrix<int> tc2(d2_sz, d2);
    dMatrix<int> tc3(tc1 * tc2);
    dMatrix<int> tc4(tc2 * tc1);

    int step = 0;

    BOOST_REQUIRE_EQUAL(tc3.size(), d1_sz);
    BOOST_REQUIRE_EQUAL(tc4.size(), d2_sz);

    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j),d3[step]);
            BOOST_CHECK_EQUAL(tc4.get(i,j),d4[step]);
            step++;
        }

}

BOOST_AUTO_TEST_CASE( um_fast_scale )
{
    const int d1 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    const int d1_size = 4;
    const int d2 [] = {2, 4,  6,  8, 
                       0, 10, 12, 14,
                       0, 0, 16, 18,
                       0, 0,  0, 20 };

    uMatrix<int> tc1(8);

    tc1.readin(d1_size, d1);

    uMatrix<int> tc2(tc1);

    tc2 *= 2;

    BOOST_REQUIRE_EQUAL(tc2.size(), d1_size);

    int count = 0;
    for(int i = 0; i < tc2.size(); i++)
        for(int j = 0; j < tc2.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc2.get(i,j), d2[count]);
            count++;
        }

}



BOOST_AUTO_TEST_SUITE_END()