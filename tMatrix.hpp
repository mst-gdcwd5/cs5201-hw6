#pragma once

template<typename T>
void tMatrix<T>::move(tMatrix<T> & mRHS){
    m_data = mRHS.m_data;
    m_size = mRHS.m_size;
    mRHS.m_data = nullptr;
    mRHS.m_size = 0;
}

template<typename T>
inline int tMatrix<T>::map(const int i, const int j) const{
    if(!valid(i,j))
        throw std::logic_error("thingy");

    return j - max(0,i - 1) + max(0,2 + (i-1)*3);
}

template<typename T>
inline bool tMatrix<T>::valid(const int i, const int j) const{
    return i >= 0 && j >= 0 && j < m_size && i < m_size;
}

template<typename T>
inline bool tMatrix<T>::valid(const int i) const{
    return i >= 0 && i < ((3 * m_size) - 2);
}

template<typename T>
inline bool tMatrix<T>::defined(const int i, const int j) const{
    return i <= j + 1 && j <= i + 1 && valid(i,j);
}

template<typename T>
tMatrix<T>::~tMatrix(){
    resize(0);
}

template<typename T>
tMatrix<T>::tMatrix(const int i){
    m_size = 0;
    resize(i);
}

template<typename T>
tMatrix<T>::tMatrix(const tMatrix<T>& r){
    m_size = 0;
    resize(r.size());

    for(int i = 0; valid(i); i++)
        element(i) = r.get(i);
    
}

template<typename T>
tMatrix<T>::tMatrix(const int size, const T data[]){
    m_size = 0;
    readin(size, data);
}

template<typename T>
tMatrix<T>& tMatrix<T>::operator=(const tMatrix<T>& r){
    resize(r.size());

    for(int i = 0; valid(i); i++){
        element(i) = r.get(i);
    }

    return *this;
}

template<typename T>
void tMatrix<T>::readin(const int size, const T data[]){
    resize(size);

    for(int i = 0; valid(i); i++)
        element(i) = data[i];
}


template<typename T>
T& tMatrix<T>::element(const int i, const int j) {
    if(!valid(i,j) || !defined(i,j))
        throw std::logic_error("invalid access");

    return m_data[map(i,j)];
}

template<typename T>
T& tMatrix<T>::element(const int i) {
    if(!valid(i))
        throw std::logic_error("invalid access");

    return m_data[i];

}

template<typename T>
T tMatrix<T>::get(const int i, const int j) const {
    if(!valid(i,j))
        throw std::logic_error("invalid access");

    if(defined(i,j))
        return m_data[map(i,j)];
    else
        return 0;
}

template<typename T>
T tMatrix<T>::get(const int i) const {
    if(!valid(i))
        throw std::logic_error("invalid access");

    return m_data[i];
}


template<typename T>
void tMatrix<T>::resize(const int i) {
    if(m_size > 0)
    {
        delete [] m_data;
        m_data = nullptr;
    }
    if(i > 0)
    {
        m_size = i;
        m_data = new T [((3 * m_size) - 2)];
    }
}

template<typename T>
int tMatrix<T>::size() const {
    return m_size;
}


template<typename T>
int tMatrix<T>::nt_row_start(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid col access");

    return max(0,i-1);

}

template<typename T>
int tMatrix<T>::nt_row_end(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid col access");

    return min(size(),i+2);
}

template<typename T>
int tMatrix<T>::nt_col_start(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return max(0,i-1);
}

template<typename T>
int tMatrix<T>::nt_col_end(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return min(size(),i + 2);
}

template<typename T>
tMatrix<T>& tMatrix<T>::operator*=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) *= r;

    return *this;
}

template<typename T>
tMatrix<T>& tMatrix<T>::operator+=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) += r;

    return *this;
}

template<typename T>
tMatrix<T>& tMatrix<T>::operator-=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) -= r;

    return *this;
}

template<typename T>
tMatrix<T>& tMatrix<T>::operator+=(const tMatrix<T>& r){

    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) += r.get(i);

    return *this;
}

template<typename T>
tMatrix<T>& tMatrix<T>::operator-=(const tMatrix<T>& r){


    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) -= r.get(i);

    return *this;
}

template<typename T>
tMatrix<T> operator*(const tMatrix<T>& l, const T& r){

    tMatrix<T> result(l);

    result *= r;

    return result;
}

template<typename T>
tMatrix<T> operator+(const tMatrix<T>& l, const tMatrix<T>& r){

    tMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
tMatrix<T> operator-(const tMatrix<T>& l, const tMatrix<T>& r){

    tMatrix<T> result(l);

    result -= r;

    return result;
}

template<typename T>
tMatrix<T> operator+(const tMatrix<T>& l,  const T& r){

    tMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
tMatrix<T> operator-(const tMatrix<T>& l,  const T& r){

    tMatrix<T> result(l);

    result -= r;

    return result;
}