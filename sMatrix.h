#pragma once

#include "aMatrix.h"

template<typename T>
class sMatrix : public aMatrix<T>
{
    T* m_data;
    int m_size;



    void move(dMatrix<T>& mRHS);

public:

    virtual inline int map(const int i, const int j) const;


    virtual inline bool valid(const int i, const int j) const;

    virtual inline bool valid(const int i) const;

    virtual inline bool defined(const int i, const int j) const;

    virtual ~sMatrix();

    sMatrix(const int i = M_DEFAULT_SIZE);

    sMatrix(const sMatrix<T>& r);

    sMatrix(const int size, const T data []);

    sMatrix(const sMatrix<T>&& r);

    virtual sMatrix<T>& operator=(const sMatrix<T>& r);

    void readin(const int size, const T data[]);

    //inherited things:

    //set&get
    virtual T& element(const int i, const int j);

    virtual T& element(const int i);

    virtual T get(const int i, const int j) const;

    virtual T get(const int i) const;

    //assumes all of these will be square
    virtual void resize(const int i);

    virtual int size() const;

    //access indecies of non-trival elements
    //how to handle for symmetric??
    virtual int nt_row_start(const int i) const;

    virtual int nt_row_end(const int i) const;

    virtual int nt_col_start(const int i) const;

    virtual int nt_col_end(const int i) const;

    //fast operators
    virtual sMatrix<T>& operator*=(const T& r);

    virtual sMatrix<T>& operator+=(const sMatrix<T>& r);

    virtual sMatrix<T>& operator-=(const sMatrix<T>& r);


};

template<typename T>
sMatrix<T> operator*(const sMatrix<T>& l, const T& r);

template<typename T>
sMatrix<T> operator+(const sMatrix<T>& l, const sMatrix<T>& r);

template<typename T>
sMatrix<T> operator-(const sMatrix<T>& l, const sMatrix<T>& r);

#include "sMatrix.hpp"