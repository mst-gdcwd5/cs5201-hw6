#pragma once

#include "tMatrix.h"
#include "lMatrix.h"

//solves ax = b for given tridiag matrix and b vector
//pre: size of vector and input matrix match
//pst: 
template<typename T>
gVector<T> solve(const tMatrix<T>& l, const gVector<T>& r);

//lower matrix decomp from symmetic matrix
//pre: operator*,+,-,/ defined for T
//pst: lower decomposed matrix returned
template<typename T>
lMatrix<T> decomp(const sMatrix<T>& l);

//solves ax = b for given symmetric matrix and b vector
//pre: size of vector and input matrix match
//pst: 
template<typename T>
gVector<T> solve(const sMatrix<T>& l, const gVector<T>& r);

//back/forward substitution
//pre: operator*,+,-,/ defined for T
//pst: 
template<typename T>
gVector<T> finish(const aMatrix<T>& l, const lMatrix<T>& m, const gVector<T>& r);

//functor for matrix solve
class mSolve
{
    public:
        //calls solve for tridiag matrix
        template<typename T>
        gVector<T> operator()(const tMatrix<T>& l, const gVector<T>& r){
            return solve(l,r);
        }

        //calls solve for symmetric matrix
        template<typename T>
        gVector<T> operator()(const sMatrix<T>& l, const gVector<T>& r){
            return solve(l,r);
        }
};


#include "solve.hpp"