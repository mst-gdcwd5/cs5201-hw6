#pragma once

#include <iostream>

#include "diagMatrix.h"
#include "sMatrix.h"
#include "tMatrix.h"
#include "uMatrix.h"
#include "lMatrix.h"

using namespace std;

//see aMatrix.h for operator* description 
template<typename T>
tMatrix<T> operator*(const diagMatrix<T>& l, const tMatrix<T>& r);

template<typename T>
tMatrix<T> operator*(const tMatrix<T>& r, const diagMatrix<T>& l );

template<typename T>
uMatrix<T> operator*(const diagMatrix<T>& l, const uMatrix<T>& r);

template<typename T>
uMatrix<T> operator*(const uMatrix<T>& r, const diagMatrix<T>& l );

template<typename T>
lMatrix<T> operator*(const diagMatrix<T>& l, const lMatrix<T>& r);

template<typename T>
lMatrix<T> operator*(const lMatrix<T>& r, const diagMatrix<T>& l );

#include "specialMult.hpp"
