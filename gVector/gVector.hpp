//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//gVector.hpp, definition of vector class

#pragma once

template<typename T>
T& gVector<T>::element(const int i) {
    if(i < size() && i >= 0)
        return m_data[i];
    else
        throw std::logic_error("invalid access");

}

template<class T>
void gVector<T>::resize(const int i)
{
    if(i < 0)
        throw std::logic_error("invalid size");

    if(m_size != 0)
    {
        if(m_size > 1)
            delete [] m_data;
        else
            delete m_data;
        m_size = 0;
    }

    if(i != 0)
    {
        m_size = i;
        m_data = new T [m_size];
    }

}

template<class T>
void gVector<T>::clear() {
    resize(0);
}

template<class T>
gVector<T>::gVector(int sz)
{
    m_size = 0;
    resize(sz);

}

template<class T>
gVector<T>::gVector(const T a, const T b, const T c)
{
    resize(3);

    element(0) = a;
    element(1) = b;
    element(2) = c;
}

template<class T>
gVector<T>::gVector(const gVector& vRHS)
{
    *this = vRHS;
}

template<class T>
gVector<T>::~gVector()
{
    resize(0);
}

template<class T>
gVector<T> & gVector<T>::operator=(const gVector<T>& vRHS)
{
    resize(vRHS.size());

    for(int i = 0; i < vRHS.size(); i++)
        element(i) = vRHS.get(i);

    return *this;
}

template<class T>
bool gVector<T>::samesz(const gVector<T> &vRHS) const
{
    return (size() == vRHS.size());
}

template<class T>
gVector<T>& gVector<T>::operator+=(const gVector<T> &vRHS)
{
    if(!samesz(vRHS))
        throw std::logic_error("not compatible for add");


    for(int i = 0; i < size(); i++)
        element(i) += vRHS.get(i);

    return *this;
}

template<class T>
gVector<T>& gVector<T>::operator-=(const gVector<T> &vRHS)
{
    (*this) += (-1 * vRHS);

    return *this;
}

template<class T>
gVector<T> gVector<T>::operator+(const gVector<T> &vRHS) const
{
    if(!samesz(vRHS))
        throw std::logic_error("not compatible for add");

    gVector<T> result(*this);

    result += vRHS;
    return result;
}

template<class T>
gVector<T> gVector<T>::operator-() const
{
    gVector<T> result(*this);

    for(int i = 0; i < size(); i++)
        result[i] *= -1;

    return result;
}

template<class T>
gVector<T> gVector<T>::operator-(const gVector<T> &vRHS) const
{
    return (*this + -vRHS);
}

template<class T>
bool gVector<T>::operator==(const gVector <T> &vRHS) const
{
    int i = 0;
    bool result = samesz(vRHS);
    while(result && i < size())
    {
        result = result && (get(i) == vRHS.get(i));
        i++;
    }

    return result;
}

template<class T>
bool gVector<T>::operator!=(const gVector <T> &vRHS) const
{
    return !(*this == vRHS);
}

template<class T>
T gVector<T>::get(const int i) const
{
    if(i < m_size && i >= 0)
        return m_data[i];
    else
        throw std::logic_error("invalid access");
}

template<class T>
T gVector<T>::operator*(const gVector<T> &vRHS) const
{
    T result;
    if(!samesz(vRHS) || size() < 1)
        throw std::logic_error("incompatible dot product");
    else
    {
        result = get(0) * vRHS.get(0);
        for(int i = 1; i < size(); i++)
            result += get(i) * vRHS.get(i);
    }
    return result;
}

template<class T, class R>
gVector<R> operator*(const T i, const gVector<R> & vRHS)
{
    gVector<R> result(vRHS);
    for(int j = 0; j < vRHS.size(); j++)
        result[j] = vRHS.get(j) * i;

    return result;
}

template<class T>
ostream& operator<<(ostream& os, const gVector<T> & vRHS)
{
    for(int i = 0; i < vRHS.size(); i++)
        os << vRHS.get(i) << " ";

    return os;
}

template<class T>
istream& operator>>(istream& is, gVector<T>& vRHS)
{
    gVector<T> build(vRHS.size());

    for(int i = 0; i < vRHS.size(); i++)
        is >> build[i];

    vRHS = build;

    return is;
}

/*
template<>
istream& operator>>(istream& is, gVector<int>& vRHS)
{
    string* input = new string[vRHS.size()];

    for(int i = 0; i < vRHS.size(); i++)
        is >> input[i];

    for(int i = 0; i < vRHS.size(); i++)
        vRHS[i] = stoi(input[i]);

    delete [] input;

    return is;
}

template<>
istream& operator>>(istream& is, gVector<float>& vRHS)
{
    string* input = new string[vRHS.size()];

    for(int i = 0; i < vRHS.size(); i++)
        is >> input[i];

    for(int i = 0; i < vRHS.size(); i++)
        vRHS[i] = stof(input[i]);

    delete [] input;

    return is;
}*/

//TODO: why do these things cause errors?



template<typename T>
void fileInput(const string& fname, vector<gVector<T>> &data)
{
    ifstream fi;
    data.clear();

    fi.open(fname);

    if(!fi.is_open())
        throw std::runtime_error("invalid file access");

    fileInput(fi, data);

    return;
}

template<typename T>
void fileInput(istream &fi, vector<gVector<T>> & data)
{
    int n;
    data.clear();
    stringstream ss;

    fi >> n;

    gVector<T> build(n);

    int i = 0;
    
    try{
        while(i < n && fi >> build)
        {
            data.push_back(build);
            i++;
        }
    }
    catch(const std::invalid_argument& e)
    {
        ss << "# invalid quaternion on line: " << (i+2);
        throw std::runtime_error(ss.str());
        
    }
    if(static_cast<int>(data.size()) != n)
        throw std::runtime_error("# insufficent number of vectors");

    return;
}

template<class T, class R>
gVector<T> operator/(const gVector<T> & vLHS, const R r)
{
    gVector<T> result(vLHS);
    
    if(r < NJ::epsilon<R>::value)
        throw std::logic_error("scale division by zero");
    

    for(int j = 0; j < vLHS.size(); j++)
        result[j] = result.get(j) / static_cast<T>(r);

    return result;
}

template<class T>
void gVector<T>::move(gVector<T> &&vRHS)
{
    resize(0);
    m_data = std::move(vRHS.m_data);
    m_size = std::move(vRHS.m_size);
    vRHS.m_data = nullptr;
    vRHS.m_size = 0;
};

template<typename T>
gVector<T>::gVector(gVector<T>&& vRHS)
{
    m_data = vRHS.m_data;
    m_size = vRHS.m_size;
    vRHS.m_data = nullptr;
    vRHS.m_size = 0;
}

template<typename T>
gVector<T> & gVector<T>::normal()
{
    if(this -> mag() > NJ::epsilon<T>::value)
    {
        *this = *this / (this -> mag());
    }
    
    return *this;
}

