#include <boost/test/unit_test.hpp>
#include "sMatrix.h"

using namespace std;

BOOST_AUTO_TEST_SUITE( sm_internal )

BOOST_AUTO_TEST_CASE( sm_construct )
{
    const int d1 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    const int d1_size = 4;
    const int d2 [] = {1, 2, 3, 4, 
                       2, 5, 6, 7,
                       3, 6, 8, 9,
                       4, 7, 9, 10 };

    sMatrix<int> tc1(d1_size, d1);

    BOOST_REQUIRE_EQUAL(tc1.size(), d1_size);

    cout << tc1 << endl;

    int count = 0;
    for(int i = 0; i < tc1.size(); i++)
        for(int j = 0; j < tc1.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc1.get(i,j), d2[count++]);
        }

}

BOOST_AUTO_TEST_CASE( sm_fast_scale )
{
    const int d1 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    const int d1_size = 4;
    const int d2 [] = {2, 4, 6, 8, 
                       4, 10, 12, 14,
                       6, 12, 16, 18,
                       8, 14, 18, 20 };

    sMatrix<int> tc1(8);

    tc1.readin(d1_size, d1);

    sMatrix<int> tc2(tc1);

    tc2 *= 2;

    BOOST_REQUIRE_EQUAL(tc2.size(), d1_size);

    int count = 0;
    for(int i = 0; i < tc2.size(); i++)
        for(int j = 0; j < tc2.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc2.get(i,j), d2[count]);
            count++;
        }

}

BOOST_AUTO_TEST_SUITE_END()