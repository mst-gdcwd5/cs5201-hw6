#pragma once

template<typename T>
void lMatrix<T>::move(dMatrix<T>& mRHS){
    m_data = mRHS.m_data;
    m_size = mRHS.m_size;
    mRHS.m_data = nullptr;
    mRHS.m_size = 0;
}

template<typename T>
inline int lMatrix<T>::map(const int i, const int j) const {
    //TODO: this could probabbly be much more efficent? yolo
    int offset = 0;
    for(int k = m_size-i; k < m_size; k++)
        offset += k;
    return j + (i*m_size) - offset;
}

template<typename T>
inline bool lMatrix<T>::valid(const int i, const int j) const {
    return i >= 0 && j >= 0 && j < m_size && i < m_size;
}

template<typename T>
inline bool lMatrix<T>::valid(const int i ) const {
    return i < ((m_size * (m_size+1))/2) && i >= 0;
}

template<typename T>
inline bool lMatrix<T>::defined(const int i, const int j) const {
    return i >= j && i >= 0;
}

template<typename T>
lMatrix<T>::~lMatrix() {
    resize(0);
}


template<typename T>
lMatrix<T>::lMatrix(const int i) {
    m_size = 0;
    resize(i);
}

template<typename T>
lMatrix<T>::lMatrix(const lMatrix<T>& r) {
    m_size = 0;
    resize(r.size());
    for(int i = 0; valid(i); i++)
        element(i) = r.get(i);
}

template<typename T>
lMatrix<T>::lMatrix(const int size, const T data[]) {
    m_size = 0;
    readin(size, data);
}

template<typename T>
lMatrix<T>& lMatrix<T>::operator=(const lMatrix<T>& r) {
    resize(r.size());
    for(int i = 0; valid(i); i++)
        element(i) = r.get(i);

    return *this;
}

template<typename T>
void lMatrix<T>::readin(const int size, const T data []) {
    resize(size);

    for(int i = 0; valid(i); i++)
        element(i) = data[i];

}

//inherited things :)

template<typename T>
T& lMatrix<T>::element(const int i, const int j) {
    if(!valid(i,j) || !defined(i,j))
        throw std::logic_error("invalid access");

    return m_data[map(i,j)];
}

template<typename T>
T& lMatrix<T>::element(const int i) {
    if(!valid(i))
        throw std::logic_error("invalid access");

    return m_data[i];

}

template<typename T>
T lMatrix<T>::get(const int i, const int j) const {
    if(!valid(i,j))
        throw std::logic_error("invalid access");

    if(defined(i,j))
        return m_data[map(i,j)];
    else
        return 0;
}

template<typename T>
T lMatrix<T>::get(const int i) const {
    if(!valid(i))
        throw std::logic_error("invalid access");

    return m_data[i];
}

template<typename T>
void lMatrix<T>::resize(const int i) {
    if(m_size > 0)
    {
        delete [] m_data;
        m_data = nullptr;
    }
    if(i > 0)
    {
        m_size = i;
        m_data = new T [((m_size * (m_size+1))/2)];
    }
}

template<typename T>
int lMatrix<T>::size() const {
    return m_size;
}

template<typename T>
int lMatrix<T>::nt_row_start(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid col access");

    return 0;
}

template<typename T>
int lMatrix<T>::nt_row_end(const int i) const {
    if(!valid(i,0))
        throw std::logic_error("invalid col access");

    return i+1;
}

template<typename T>
int lMatrix<T>::nt_col_start(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return i;
}

template<typename T>
int lMatrix<T>::nt_col_end(const int i) const {
    if(!valid(0,i))
        throw std::logic_error("invalid col access");

    return m_size;
}

template<typename T>
lMatrix<T>& lMatrix<T>::operator*=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) *= r;

    return *this;
}

template<typename T>
lMatrix<T>& lMatrix<T>::operator+=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) += r;

    return *this;
}

template<typename T>
lMatrix<T>& lMatrix<T>::operator-=(const T& r){
    for(int i = 0; valid(i); i++)
        element(i) -= r;

    return *this;
}

template<typename T>
lMatrix<T>& lMatrix<T>::operator+=(const lMatrix<T>& r){


    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) += r.get(i);

    return *this;
}

template<typename T>
lMatrix<T>& lMatrix<T>::operator-=(const lMatrix<T>& r){


    if(size() != r.size())
        throw std::logic_error("invalid dimensions");

    for(int i = 0; valid(i); i++)
        element(i) -= r.get(i);

    return *this;
}


template<typename T>
lMatrix<T> operator*(const lMatrix<T>& l, const T& r){

    lMatrix<T> result(l);

    result *= r;

    return result;
}

template<typename T>
lMatrix<T> operator*(const lMatrix<T>& l, const lMatrix<T>& r){


    int size = l.size();

    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    lMatrix<T> result(l);

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            result.element(i,j) = 0;
            int st = max(l.nt_row_start(i),r.nt_col_start(j));
            int end = min(l.nt_row_end(i),r.nt_col_end(j));
            for(int k = st; k < end; k++)
            {
                ops++;
                result.element(i,j) += l.get(i,k) * r.get(k,j);
            }
        }

    cout << "# [" << l.size() << "," << r.size() <<"] operations: " << ops << endl;

    return result;
}

template<typename T>
lMatrix<T> operator+(const lMatrix<T>& l, const lMatrix<T>& r){

    lMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
lMatrix<T> operator-(const lMatrix<T>& l, const lMatrix<T>& r){

    lMatrix<T> result(l);

    result -= r;

    return result;
}

template<typename T>
lMatrix<T> operator+(const lMatrix<T>& l, const T& r){

    lMatrix<T> result(l);

    result += r;

    return result;
}

template<typename T>
lMatrix<T> operator-(const lMatrix<T>& l, const T& r){

    lMatrix<T> result(l);

    result -= r;

    return result;
}