#pragma once

template<typename T>
gVector<T> solve(const tMatrix<T>& l, const gVector<T>& r){
    
    if(l.size() != r.size())
        throw std::logic_error("spicy boys");

    const int sz = l.size();
    
    tMatrix<T> lp(l);
    gVector<T> rp(r);
    gVector<T> x(r);
    int i, j;

    i = 0;
    j = 1;

    while(l.valid(i,j))
    {
        if(i == 0){
            lp.element(i,j) = l.get(i,j) / l.get(i,j-1);
        }
        else{
            lp.element(i,j) = l.get(i,j) / (l.get(i,j-1) - (l.get(i,j-2)*lp.get(i-1,j-1)));
        }


        i++;
        j++;
    }


    i = 0;
    while(i < r.size())
    {
        if(i == 0)
            rp[i] = r.get(i) / l.get(i,i);
        else
        {
            rp[i] = ((r.get(i) - (l.get(i,i-1)*rp.get(i-1))));
            rp[i] /= (l.get(i,i) - (l.get(i,i-1)*lp.get(i-1,i)));
        }
            
        i++;
    }


    i = sz - 1;
    while(i >= 0)
    {
        x[i] = rp.get(i);

        if(i < sz - 1)
            x[i] -= lp.get(i,i+1) * x.get(i+1);

        i--;
    }

    return x;
}

template<typename T>
lMatrix<T> decomp(const sMatrix<T>& l){


    const int sz = l.size();
    
    lMatrix<T> m(sz);

    for(int k = 0; k < sz; k++)
    {
        for(int i = 0; i < k; i++)
        {
            m.element(k,i) = l.get(k,i);
            for(int j = 0; j < i; j++)
                m.element(k,i) -= m.get(i,j) * m.get(k,j);
            m.element(k,i) /= m.get(i,i);
        }
        m.element(k,k) = l.get(k,k);
        for(int j = 0; j < k; j++)
            m.element(k,k) -= static_cast<T>(pow(m.get(k,j),2));
        m.element(k,k) = sqrt(m.get(k,k));
    }

    return m;
}

template<typename T>
gVector<T> solve(const sMatrix<T>& l, const gVector<T>& r){
    
    lMatrix<T> m = decomp(l);

    auto x = finish(l, m, r);

    return x;
}

template<typename T>
gVector<T> finish(const aMatrix<T>& l, const lMatrix<T>& m, const gVector<T>& r){

    const int sz = m.size();

    gVector<T> d(sz);

    gVector<T> x(sz);

    //forward sub
    for(int i = 0; i < sz; i++){
        d[i] = r.get(i);
        for(int j = 0; j < i; j++)
            d[i] -= l.get(i,j) * d.get(j);
    }

    //back sub
    for(int i = sz -1; i >= 0; i--){
        x[i] = d.get(i);
        for(int j = i+1; j < sz; j++)
            x[i] -= l.get(i,j) * x[j];
        x[i] /= l.get(i,i);
    }

    return x;
}