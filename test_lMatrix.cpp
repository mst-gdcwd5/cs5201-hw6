#include <boost/test/unit_test.hpp>
#include "lMatrix.h"

using namespace std;

BOOST_AUTO_TEST_SUITE( lm_internal )

BOOST_AUTO_TEST_CASE( lm_construct )
{
    const int d1 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    const int d1_size = 4;
    const int d2 [] = {1, 0, 0, 0, 
                       2, 3, 0, 0,
                       4, 5, 6, 0,
                       7, 8, 9, 10 };

    lMatrix<int> tc1(d1_size, d1);


    BOOST_REQUIRE_EQUAL(tc1.size(), d1_size);

    cout << tc1 << endl;

    int count = 0;
    for(int i = 0; i < tc1.size(); i++)
        for(int j = 0; j < tc1.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc1.get(i,j), d2[count++]);
        }

}

BOOST_AUTO_TEST_CASE( lm_fast_scale )
{
    const int d1 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    const int d1_size = 4;
    const int d2 [] = {2, 0, 0, 0, 
                       4, 6, 0, 0,
                       8, 10, 12, 0,
                       14, 16, 18, 20 };

    lMatrix<int> tc1(8);

    tc1.readin(d1_size, d1);

    lMatrix<int> tc2(tc1);

    tc2 *= 2;

    BOOST_REQUIRE_EQUAL(tc2.size(), d1_size);

    int count = 0;
    for(int i = 0; i < tc2.size(); i++)
        for(int j = 0; j < tc2.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc2.get(i,j), d2[count]);
            count++;
        }

}

BOOST_AUTO_TEST_SUITE_END()